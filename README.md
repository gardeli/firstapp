Django
=======

Projeto de criacao firstapp usando Django.

Baseado no [djangoproject.com][0].

# Etapas do tutorial

Guia de Instalacao e configuracao do Django - https://docs.djangoproject.com/en/3.0/intro/install/

1. - https://docs.djangoproject.com/en/3.0/intro/tutorial01/ - criação de projeto

2. - https://docs.djangoproject.com/en/3.0/intro/tutorial02/ - database/models

3. - https://docs.djangoproject.com/en/3.0/intro/tutorial03/ - views/urls

4. - https://docs.djangoproject.com/en/3.0/intro/tutorial04/ - form/templates

5. - https://docs.djangoproject.com/en/3.0/intro/tutorial05/ - tests

6. - https://docs.djangoproject.com/en/3.0/intro/tutorial06/ -  CSS/static

7. - https://docs.djangoproject.com/en/3.0/intro/tutorial07/ - Admin

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).